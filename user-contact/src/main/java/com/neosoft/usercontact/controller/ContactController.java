package com.neosoft.usercontact.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.neosoft.usercontact.convertor.ContactConvertor;
import com.neosoft.usercontact.dto.ContactDTO;
import com.neosoft.usercontact.entity.Contact;
import com.neosoft.usercontact.repository.ContactRepositoty;
import com.neosoft.usercontact.service.ContactService;

@RestController
@RequestMapping("/api")
public class ContactController {
	

	private static final Logger logger=LoggerFactory.getLogger(ContactController.class);
	 
	@Autowired
	ContactConvertor contactConvertor;
    
	@Autowired
	ContactRepositoty contactRepositoty;

	@Autowired
	Gson gson;
	
	@Autowired
	ContactService contactService;
	
	@GetMapping("/contact")
	public ResponseEntity<List> getContact()
	{
		List<Contact> Contacts=null;
		try {
			Contacts=contactRepositoty.findAll();
			List<ContactDTO>	response=Contacts.stream().map(this::convertToResponse).collect(Collectors.toList());
			return new ResponseEntity<List>(response,HttpStatus.OK);
		}catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List>(java.util.Arrays.asList("some thing went wrong"),HttpStatus.OK);
		}
		
		
	}
	
	public ContactDTO convertToResponse(Contact entity)
	{
		
		ContactDTO dto=new ContactDTO();
		dto.setAddress(entity.getAddress());
		dto.setState(entity.getState());
		dto.setCity(entity.getCity());
		dto.setDist(entity.getDist());
		dto.setPincode(entity.getPincode());
		
		
		return dto;
		
	}

	@PostMapping("/contact")
	public ResponseEntity<String> addContact(@Valid @RequestBody ContactDTO dto,
			HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest) {
		Contact Contact = null;
		try {
			Contact = contactConvertor.dtoToEntity(dto);
			contactService.saveContact(Contact);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResponseEntity<>(gson.toJson("some thing went wrong"), HttpStatus.CREATED);
		}
		return new ResponseEntity<>(gson.toJson(Contact), HttpStatus.CREATED);
	}
	
	
	@PatchMapping(value = "/contact/{id}")
	public ResponseEntity<String> getcontactById(@PathVariable Integer id, HttpServletRequest request,
			HttpServletResponse response) {
		ContactDTO userDTO = null;
		logger.info("-- PATCH mapping call--");
		try {

			Contact user = contactService.getcontactById(id);
			if (user != null) {
				userDTO = contactConvertor.entityToDTO(user);
				response.setStatus(HttpStatus.OK.value());
				logger.info("-- PATCH mapping call successfully--");
				return new ResponseEntity<>(gson.toJson(userDTO), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(gson.toJson("User Not Available"), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Map<String, String> map = new HashMap<String, String>();
			map.put("status", "Bad Request");
			map.put("message", "Record not found for this id");
			return new ResponseEntity<>(gson.toJson(map), HttpStatus.BAD_REQUEST);
		}
		

	}

	// update the data by using id
	@PutMapping(value = "/contact/{id}")
	public ResponseEntity<String> updateContact(@RequestBody ContactDTO dto, @PathVariable Integer id,
			HttpServletRequest request, HttpServletResponse response) {
		try {

			Contact user = contactService.getcontactById(id);
			if (user != null) {
				user = contactConvertor.entityToDTO(user, dto);
				contactRepositoty.save(user);
				response.setStatus(HttpStatus.OK.value());
				return new ResponseEntity<>(gson.toJson(user), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(gson.toJson("User Not Available"), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(gson.toJson("Something went wrong"), HttpStatus.BAD_REQUEST);
		}
		

	}

	// hard delete the data by using id
	@DeleteMapping(value = "/contact/{id}")
	public ResponseEntity<String> deleteContact(@PathVariable("id") Integer id) {
		try {

			Contact user = contactService.getcontactById(id);
			if (user != null) {
				contactRepositoty.deleteById(user.getId());
				logger.info("--hard delete call successfully--" + id);
				return new ResponseEntity<>(gson.toJson("User delete successfully"), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(gson.toJson("User Not Available"), HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(gson.toJson("Some thing is wrong"), HttpStatus.BAD_REQUEST);
		}

	}

}
