package com.neosoft.usercontact.convertor;

import org.springframework.stereotype.Component;

import com.neosoft.usercontact.dto.ContactDTO;
import com.neosoft.usercontact.entity.Contact;

@Component
public class ContactConvertor {

	public Contact  dtoToEntity(ContactDTO dto)
	{
		Contact contact=Contact.builder().address(dto.getAddress()).state(dto.getState()).city(dto.getCity()).dist(dto.getDist()).pincode(dto.getPincode()).build();
		return contact;
	}
	public ContactDTO  entityToDTO(Contact entity)
	{
		ContactDTO dto=ContactDTO.builder().address(entity.getAddress()).state(entity.getState()).city(entity.getCity()).dist(entity.getDist()).pincode(entity.getPincode()).build();
		return dto;
	}
	
	public Contact  entityToDTO(Contact entity,ContactDTO dto)
	{
	  	entity.setAddress(dto.getAddress());
	  	entity.setState(dto.getState());
	  	entity.setDist(dto.getDist());
	  	entity.setPincode(dto.getPincode());
	  	entity.setUserId(dto.getUserId());
	  	entity.setCity(dto.getCity());
	  	
	  	return entity;
	}
}
