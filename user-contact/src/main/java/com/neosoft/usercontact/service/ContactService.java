package com.neosoft.usercontact.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.neosoft.usercontact.entity.Contact;

@Service
public interface ContactService {

	Contact saveContact(Contact contact);
	
	Contact getcontactById(Integer id);
	
	List<Contact> getContactByUserId(String userId);
}
