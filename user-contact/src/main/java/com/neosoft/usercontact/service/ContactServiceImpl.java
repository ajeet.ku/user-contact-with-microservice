package com.neosoft.usercontact.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neosoft.usercontact.entity.Contact;
import com.neosoft.usercontact.repository.ContactRepositoty;

@Service
public class ContactServiceImpl implements ContactService{
	
	@Autowired
	ContactRepositoty contactRepositoty;

	@Override
	public Contact saveContact(Contact contact) {
		// TODO Auto-generated method stub
		return contactRepositoty.save(contact);
	}

	@Override
	public Contact getcontactById(Integer id) {
		// TODO Auto-generated method stub
		return contactRepositoty.findById(id).orElse(null);
	}

	@Override
	public List<Contact> getContactByUserId(String userId) {
		// TODO Auto-generated method stub
		return contactRepositoty.findByUserId(userId);
	}

}
