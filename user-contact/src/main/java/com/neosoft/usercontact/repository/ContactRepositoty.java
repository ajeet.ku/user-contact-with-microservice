package com.neosoft.usercontact.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.usercontact.entity.Contact;

@Repository
public interface ContactRepositoty extends MongoRepository<Contact, Integer>{
	
	List<Contact> findByUserId(String userId);

}
