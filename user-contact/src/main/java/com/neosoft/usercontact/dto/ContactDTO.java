package com.neosoft.usercontact.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactDTO {
	
	private Integer id;
	private String address;
	private String state;
	private String dist;
	private String city;
	private Integer pincode;
	private String userId;

}
